from database import get_indicators_value_for_cities
from sources import indicator_aid_to_id, indicator_id_to_aid, indicator_aids
from itertools import groupby
from operator import itemgetter
import fnmatch
import json
import csv

indicator_getter = itemgetter(0)
city_getter = itemgetter(1)
value_getter = itemgetter(2)

flatten = lambda l: [item for sublist in l for item in sublist]

with open('selected.json') as jf:
    j = json.loads(jf.read())

def get_aggregate_deps(expr):
    if type(expr) is str:
        if expr.startswith('?'):
            if expr in j['variables']:
                return get_aggregate_deps(j['variables'][expr])
            else:
                return []
        else:
            return fnmatch.filter(indicator_aids, expr)
    return flatten(get_aggregate_deps(op) for op in expr['ops'])

def get_aggregate_id(expr):
    if type(expr) is str: return expr
    if 'id' in expr: return expr['id']
    expr['id'] = f"{expr['op']}({','.join(get_aggregate_id(op) for op in expr['ops'])})"
    return expr['id']

print("Compiling list of indicator ids we need to fetch from the database...")
wanted_indicator_ids = set()
for aggregate in j['aggregates']:
    indicator_aid_deps = get_aggregate_deps(aggregate)
    for indicator_aid in indicator_aid_deps:
        if indicator_aid not in indicator_aid_to_id:
            raise Exception(f"{indicator_aid} doesn't exist")
        indicator_id = indicator_aid_to_id[indicator_aid]
        wanted_indicator_ids.add(indicator_id)
print("Done! We need to fetch: ")
for indicator_id in sorted(wanted_indicator_ids):
    print(f" * {indicator_id}")

print("Getting data from the database...")
city_ids = j['cities']
indicators_cities_values = get_indicators_value_for_cities(list(wanted_indicator_ids), city_ids)
by_indicator_id = dict((key, list(value)) for key, value in groupby(indicators_cities_values, key=indicator_getter))

print("Processing indicators...")
variables = j['variables']

def aggregate_sum(op1, op2):
    if op1 is None or op2 is None: return None
    return [float(a) + float(b) for a, b in zip(op1, op2)]

def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def aggregate_div(op1, op2):
    if op1 is None or op2 is None: return None
    return [
        (float(a) / float(b)) if isfloat(a) else '' for a, b in zip(op1, op2)
    ]
operations = { 'sum': aggregate_sum, 'div': aggregate_div, }

calculated_values = dict()

def get_cities_values_for_aggregate(aggregate):
    if type(aggregate) is str:
        if aggregate.startswith('?'):
            if aggregate in variables:
                return get_cities_values_for_aggregate(variables[aggregate])
            else:
                print(f"Warning: Variable {aggregate} not defined")
                return None
        if aggregate in calculated_values:
            return calculated_values[aggregate]

        values = []
        for indicator_aid in fnmatch.filter(indicator_aids, aggregate):
            indicator_id = indicator_aid_to_id[indicator_aid]
            if indicator_id not in by_indicator_id: continue
            by_cities = groupby(by_indicator_id[indicator_id], key=city_getter)
            by_cities = dict((key, next(value)) for key, value in by_cities)
            values += [value_getter(by_cities[city_id]) for city_id in city_ids if city_id in by_cities]

        calculated_values[aggregate] = values
        return values

    if 'value' in aggregate:
        return aggregate['value']
    if 'visited' in aggregate:
        raise Exception('Dependency loop found')

    aggregate['visited'] = True

    operation = operations[aggregate['op']]
    ops = list(map(get_cities_values_for_aggregate, aggregate['ops']))
    aggregate['value'] = operation(*ops)
    return aggregate['value']

rows = []
for aggregate in j['aggregates']:
    cities_values = get_cities_values_for_aggregate(aggregate)
    aggregate_id = get_aggregate_id(aggregate)
    if cities_values is None or len(get_aggregate_deps(aggregate)) == 0:
        rows.append([aggregate_id])
    else:
        aggregate_id = ':' + aggregate_id
        rows.append([aggregate_id] + [str(value).replace('.', ',') for value in cities_values])

with open('result.csv', 'w', encoding='utf-8', newline='') as cf:
    c = csv.writer(cf)
    c.writerow(['indicator_aid'] + city_ids)
    for row in rows:
        c.writerow(row)
