from pprint import pprint
import json
import requests

indicadores = dict()
def dfs_metadata(root):
    if root['classe'] == 'I':
        indicadores['SDIBGE' + str(root['id'])] = root['posicao'] + ' ' + root['indicador']
    for child in root['children']:
        dfs_metadata(child)

res = requests.get(f'https://servicodados.ibge.gov.br/api/v1/pesquisas/1/periodos/2001/indicadores/0/')
for indicadores_raw in res.json():
    dfs_metadata(indicadores_raw)

metadata = { 'indicators': [] }
with open('metadata.csv', 'w') as f:
    for sdibgeid, nome in indicadores.items():
        metadata['indicators'].append({ 'id': sdibgeid, 'name': nome })
        f.write(f'{sdibgeid}\t{nome}\n')

with open('metadata.json', 'w') as f:
    f.write(json.dumps(metadata, ensure_ascii=False, indent='  '))
