from download import download_and_prepare_file
from workbook import load_workbook_source_into_database
from database import is_imported
from sources import sources
import json

if __name__ == '__main__':
    for source in sources:
        filepath, file_format = download_and_prepare_file(source)
