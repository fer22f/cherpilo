import os
import hashlib
from download import get_md5sum

for filename in os.listdir('download/'):
    if filename.startswith('.'):
        continue
    md5sum = get_md5sum(os.path.join('download/', filename))
    expected_md5sum = filename.split('.')[0]
    if md5sum != expected_md5sum:
        print(filename, '->', md5sum)
