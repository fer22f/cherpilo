from download import download_and_prepare_file
from loader import load_source_into_database
from database import is_imported
from sources import sources
import json

if __name__ == '__main__':
    for source in sources:
        if is_imported(source['id']):
            print(f"{source['id']}: Already imported")
            continue
        print(f"{source['id']}: Downloading {source['url']}")
        filepath, file_format = download_and_prepare_file(source)

        if 'workbook' in source or 'extractor' in source or source['format'] == 'csv' or 'query' in source:
            print(f"{source['id']}: Loading into the database...")
            load_source_into_database(source, file_format, filepath)
        else:
            print(f"{source['id']}: Warning: No workbook or extractor found")
