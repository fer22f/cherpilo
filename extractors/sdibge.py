from pprint import pprint
from database import get_city_ids, get_city_count
from extra_iter import grouper
from bisect import bisect_left
import csv
import json
import requests

def extractor(output_filepath, url, years, cb):
    city_ids = get_city_ids()
    city_count = get_city_count()

    with open(output_filepath, 'w', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(['indicator_id', 'year', 'city_id', 'value'])

        i = 0
        cb(i, city_count)
        for city_ids_group in grouper(city_ids, 796):
            city_ids_group_without_verification = list(str(city_id)[:-1] for city_id in city_ids_group)
            res = requests.get(f'{url}{"|".join(city_ids_group_without_verification)}')
            indicators = res.json()
            for indicator in indicators:
                indicator_id = f'SDIBGE{indicator["id"]}'
                rows = indicator['res']
                for row in rows:
                    city_id_without_verification = row['localidade']
                    j = bisect_left(
                        city_ids_group_without_verification,
                        city_id_without_verification)
                    city_id = city_ids_group[j]
                    for year in years:
                        writer.writerow([indicator_id, year, city_id, row['res'][str(year)]])
            i += len(city_ids_group)
            cb(i, city_count)
