from bs4 import BeautifulSoup
import requests
import csv

url = lambda offset: f'http://www.sei.ba.gov.br/side/alimenta.wsp?tmp.tabela=T164&tmp.conector.offset={offset}'

with open('side.csv', 'w', encoding='utf-8') as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(["Município", "Ano", "Índice", "Valor", "Ranking"])
    for i in range(0, 3753, 500):
        print(i)
        soup = BeautifulSoup(requests.get(url(i)).text, 'html.parser')
        for rows in soup.select('tr[bgcolor="#ffffff"]'):
            children = list(rows.children)
            if len(children) == 5:
                # this has more information
                municipio = children[0].string
                ano = children[1].string
                indice = children[2].string
                valor = children[3].string
                ranking = children[4].string
            elif len(children) == 3:
                indice = children[0].string
                valor = children[1].string
                ranking = children[2].string

            writer.writerow([municipio, ano, indice, valor, ranking])
