from collections import defaultdict
from pprint import pprint
from glob import glob
import os
import json

indicator_aid_to_id = dict()
indicator_id_to_aid = dict()
indicator_aids = list()

sources_tree = { 'children': dict() }
sources = []

def add_tree(path, name, root):
    for filename in os.listdir(path):
        full_path = os.path.join(path, filename)
        if os.path.isdir(full_path):
            root['children'][filename] = {
                'id': name + '/' + filename,
                'children': dict()
            }
            add_tree(full_path, name + '/' + filename, root['children'][filename])
        elif filename.endswith('.json'):
            with open(full_path) as f:
                try:
                    parsed = json.loads(f.read())
                except:
                    raise Exception('JSON malformado em ' + full_path)

                if filename == 'index.json':
                    root.update(parsed)
                else:
                    source_id = name + '/' + filename[:-5]
                    parsed['id'] = source_id
                    root['children'][filename[:-5]] = parsed
                    sources.append(parsed)

add_tree('sources', '', sources_tree)

for source in sources:
    for year in source['years']:
        for indicator in source.get('indicators', []):
            indicator_aid = f"{source['id']}/{year}/{indicator.get('tid', indicator['id'])}"
            indicator_id = f"{source['id']}/{year}/{indicator['id']}"
            indicator_aid_to_id[indicator_aid] = indicator_id
            indicator_aid_to_id[indicator_id] = indicator_aid
            indicator_aids.append(indicator_aid)
