from zipfile import ZipFile
from urllib.request import urlopen, Request
import os
import shutil
import hashlib
import extractors.sdibge

EXTRACTORS = {
    'sdibge': extractors.sdibge.extractor
}

def get_md5sum(filepath):
    BUF_SIZE = 65536
    md5 = hashlib.md5()
    with open(filepath, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            md5.update(data)
    return md5.hexdigest()

def unzip_file(src, filename, dest):
    with ZipFile(src) as z:
        with z.open(filename) as zf, open(dest, "wb") as f:
            shutil.copyfileobj(zf, f)

USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36'

def download_file(dest, url, cb):
    response = urlopen(Request(url, headers={'User-Agent': USER_AGENT}))
    CHUNK = 16 * 1024
    with open(dest, 'wb') as f:
        i = 0
        while True:
            cb(i, response.headers['content-length'])
            chunk = response.read(CHUNK)
            if not chunk:
                break
            i += len(chunk)
            f.write(chunk)

def download_and_prepare_file(source):
    file_format = source.get('format', 'csv')
    main_filepath = f'download/{source["md5"]}.{file_format}'
    print(f"Downloading {source['url']}...")
    file_exists = os.path.isfile(main_filepath)
    if not file_exists:
        print("File doesn't exist, downloading...")
    if file_exists:
        md5_matches = get_md5sum(main_filepath) == source['md5']
        if not md5_matches:
            print("File currently downloaded is corrupted, downloading it again...")

    if file_format.endswith('.zip') and not file_exists:
        zip_in_file_filepath = f'download/{source["md5FileInZip"]}.{source["format"][:-4]}'
        if os.path.isfile(zip_in_file_filepath):
            md5_matches = get_md5sum(zip_in_file_filepath) == source['md5FileInZip']
            if md5_matches:
                print("The unzipped version is already here, no need to download!")

    if not file_exists or not md5_matches:
        if 'extractor' in source:
            EXTRACTORS[source['extractor']](main_filepath, source['url'], source['years'], lambda i, m: print(i, m))
        else:
            download_file(main_filepath, source['url'], lambda i, m: print(i, m))
    else:
        print("File already downloaded and signature matches")

    if file_format in ('xlsx', 'xls', 'csv'):
        return main_filepath, file_format
    elif file_format.endswith('zip'):
        file_format = file_format[:-4]
        zip_in_file_filepath = f'download/{source["md5FileInZip"]}.{file_format}'
        unzipped_file_exists = os.path.isfile(zip_in_file_filepath)
        if unzipped_file_exists:
            md5_matches = get_md5sum(zip_in_file_filepath) == source['md5FileInZip']
            if md5_matches:
                print("The unzipped version is already here, no need to unzip!")
        if not unzipped_file_exists or not md5_matches:
            print("Unzipping file...")
            unzip_file(main_filepath, source["fileInZip"], zip_in_file_filepath)
        return zip_in_file_filepath, file_format
    else:
        print("Unsupported file format for downloading")
