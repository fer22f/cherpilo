from database import batch_insert_indicator_city, get_city_by_name_uf_initials, get_city_ids
from city import load_cities_into_database
import pyexcel
from download import download_and_prepare_file
from bisect import bisect_left
from access import get_rows_from_access

load_cities_into_database()

city_ids = [str(n) for n in get_city_ids()]
city_ids_without_verification = list(city_id[:-1] for city_id in city_ids)

def year_as_column(rows, indicators, years, year_column, city_id_column, city_column, indicator_start_column, indicator_row, first_city_id, last_city_id):
    first_row = True
    expected_row_length = indicator_start_column + len(indicators)
    for row in rows:
        for j, cell in enumerate(row):
            if j == year_column:
                current_year = cell
            elif j == city_column:
                city_name, city_uf_initials = cell.split(' - ')
                city_name = city_name.strip()
                current_city_id = get_city_by_name_uf_initials(city_name, city_uf_initials)
                if current_city_id is None:
                    raise Exception(f"Failed to find city '{city_name}' - '{city_uf_initials}' from cell '{cell}'")
                if first_row and str(int(current_city_id)) != str(first_city_id):
                    raise Exception(f"Found '{str(int(current_city_id))}' but expected first city id to be '{str(first_city_id)}'")
            elif j == city_id_column:
                current_city_id = cell
                if len(str(current_city_id)) == 6:
                    k = bisect_left(
                        city_ids_without_verification,
                        str(current_city_id))
                    current_city_id = city_ids[k]
                if first_row and str(int(current_city_id)) != str(first_city_id):
                    raise Exception(f"Found '{str(int(current_city_id))}' but expected first city id to be '{str(first_city_id)}'")
            elif j >= indicator_start_column:
                indicator = indicators[j - indicator_start_column]
                indicator_id = indicator["id"]
                indicator_row_id = indicator.get("indicatorRowId", indicator_id)
                if indicator_row is not None and indicator_row[j] != indicator_row_id:
                    raise Exception(f"Found {repr(indicator_row[j])} but expected indicator_row_id to be {repr(indicator_row_id)}")
                yield (f'{current_year}/{indicator_id}', current_city_id, cell)
        first_row = False
        if len(row) > expected_row_length:
            raise Exception(f"Found row length of {len(row)} but expected it to be {expected_row_length}")
    if str(int(current_city_id)) != str(last_city_id):
        raise Exception(f"Found '{str(int(current_city_id))}' but expected last city id to be '{str(last_city_id)}'")

def year_cycle(rows, indicators, years, city_id_column, city_column, skip_districts, skip_rows, indicator_start_column, indicator_row, first_city_id, last_city_id, ignore_last_columns):
    first_row = True
    expected_row_length = indicator_start_column + len(indicators)*len(years) + ignore_last_columns
    any_city_column = city_id_column if city_id_column is not None else city_column
    any_city_column = max(any_city_column) if isinstance(any_city_column, list) else any_city_column
    if any_city_column > indicator_start_column:
        expected_row_length += 1
        ignore_last_columns += 1

    for i, row in enumerate(rows):
        if i in skip_rows:
            if len(row) >= any_city_column:
                print(row)
                raise Exception("Can't skip row with data!")
            continue
        if len(row) < any_city_column:
            print(i, row)
            raise Exception("Row should be skipped")
        if city_id_column is not None:
            if isinstance(city_id_column, list):
                cell = f'{row[city_id_column[0]]}{row[city_id_column[1]]:0>4}'
            else:
                cell = row[city_id_column]

            current_city_id = cell
            if len(str(int(current_city_id))) == 6:
                k = bisect_left(
                    city_ids_without_verification,
                    str(int(current_city_id)))
                current_city_id = city_ids[k]
            elif len(str(int(current_city_id))) == 7:
                pass
            elif len(str(int(current_city_id))) == 9 or len(str(int(current_city_id))) == 11:
                if not skip_districts:
                    raise Exception("Please turn on skipping of districts")
                continue
            else:
                raise Exception(f"Invalid city id: {current_city_id}")

            if first_row and str(int(current_city_id)) != str(first_city_id):
                raise Exception(f"Found '{str(int(current_city_id))}' but expected first city id to be '{str(first_city_id)}'")
        else:
            cell = row[city_column]
            city_name, city_uf_initials = cell.split(' - ')
            city_name = city_name.strip()
            current_city_id = get_city_by_name_uf_initials(city_name, city_uf_initials)
            if current_city_id is None:
                raise Exception(f"Failed to find city '{city_name}' - '{city_uf_initials}' from cell '{cell}'")
            if first_row and str(int(current_city_id)) != str(first_city_id):
                raise Exception(f"Found '{str(int(current_city_id))}' but expected first city id to be '{str(first_city_id)}'")

        for j, cell in enumerate(row):
            if j >= indicator_start_column and j < len(row) - ignore_last_columns:
                offset = (j - indicator_start_column)
                indicator = indicators[offset % len(indicators)]
                current_indicator_id = indicator["id"]
                expected_indicator_row_id = indicator.get("indicatorRowId", current_indicator_id)
                current_value = cell
                if indicator_row is not None:
                    actual_indicator_row_id = indicator_row[j]
                    if type(actual_indicator_row_id) is str:
                        actual_indicator_row_id = actual_indicator_row_id.strip()
                    if actual_indicator_row_id != expected_indicator_row_id:
                        raise Exception(f"Found {repr(actual_indicator_row_id)} but expected indicator_row_id to be {repr(expected_indicator_row_id)}")
                current_year = years[offset // len(indicators)]
                yield (f'{current_year}/{current_indicator_id}', current_city_id, current_value)

        if len(row) > expected_row_length:
            raise Exception(f"Found row length of {len(row)} but expected it to be {expected_row_length}")

        first_row = False
    if str(int(current_city_id)) != str(last_city_id):
        raise Exception(f"Found '{str(int(current_city_id))}' but expected last city id to be '{str(last_city_id)}'")

def indicator_year_city_value(rows):
    for indicator_id, year, city_id, value in rows:
        yield (f'{year}/{indicator_id}', city_id, value)

def get_rows_from_workbook_like(source, file_format, filename):
    args = {
        'file_name': filename,
        'sheet_name': source.get('workbook'),
        'encoding': 'utf-8'
    }
    if file_format != 'csv':
        args['skip_hidden_row_and_column'] = False

    indicator_row = next(
        pyexcel.iget_array(**args, start_row=source['indicatorRow'], row_limit=1)
    ) if 'extractor' not in source and source['indicatorRow'] is not None else None
    rows = pyexcel.iget_array(
        **args,
        start_row=source.get('startRow', 1 if file_format == 'csv' else 0),
        row_limit=source['endRow'] - source['startRow'] if 'endRow' in source else -1
    )
    return indicator_row, rows

def load_source_into_database(source, file_format, filename):
    if file_format not in ('accdb', 'mdb'):
        indicator_row, rows = get_rows_from_workbook_like(source, file_format, filename)
    else:
        rows = get_rows_from_access(source, file_format, filename)
        indicator_row = next(rows)

    if 'extractor' in source:
        generator = indicator_year_city_value(rows)
    elif source.get('yearCycle', False) or len(source['years']) == 1:
        generator = year_cycle(rows,
            indicators=source['indicators'],
            years=source['years'],
            city_id_column=source.get('cityIdColumn'),
            city_column=source.get('cityColumn'),
            indicator_start_column=source['indicatorStartColumn'],
            indicator_row=indicator_row,
            skip_districts=source.get('skipDistricts', False),
            skip_rows=set(source.get('skipRows', [])),
            first_city_id=source['firstCityId'],
            last_city_id=source['lastCityId'],
            ignore_last_columns=source.get('ignoreLastColumns', 0))
    else:
        generator = year_as_column(rows,
            years=source['years'],
            indicators=source['indicators'],
            year_column=source['yearColumn'],
            city_id_column=source.get('cityIdColumn'),
            city_column=source.get('cityColumn'),
            indicator_start_column=source['indicatorStartColumn'],
            indicator_row=indicator_row,
            first_city_id=source['firstCityId'],
            last_city_id=source['lastCityId'])

    batch_insert_indicator_city(source['id'], generator)
    pyexcel.free_resources()
