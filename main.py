from tkinter import *
from tkinter.ttk import *
from ttkwidgets import CheckboxTreeview
import json

from download import download_file
from loader import load_source_into_database
from sources import sources_tree

import os

class Application(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.queue = list()
        self.pack(fill="both", expand=True)
        self.create_widgets()

    def update_progress_bar(self, i, j):
        self.progressbar['value'] = i
        self.progressbar['maximum'] = j
        self.update()

    def load_source_into_database(self, source):
        load_source_into_database(source, f'download/{source["md5"]}.{source["format"]}')

    def download(self):
        metadata = sources[0]
        download_file(
            metadata['url'],
            f'download/{source["md5"]}.{source["format"]}',
            self.update_progress_bar
        )
        self.load_source_into_database(metadata)

    def filter_tree(self, search_text, el='', dont_detach=False):
        children = self.tree_view.get_children(el)

        item = self.tree_view.item(el)
        values = list(item.values())
        contains_search_text = (
            search_text in str(item['text']) or
            search_text in str(values[0]) or
            search_text in str(values[1]) or
            search_text in str(values[2])
        )

        any_sucessful = any(
            self.filter_tree(search_text, child, contains_search_text)
            for child in children
        )

        if contains_search_text or any_sucessful or dont_detach:
            return True
        else:
            if el != '':
                self.queue.append((el, self.tree_view.parent(el), self.tree_view.index(el)))
                self.tree_view.detach(el)
            return False

    def reattach_tree(self):
        while len(self.queue):
            self.tree_view.reattach(*self.queue.pop())

    def update_search(self, name, index, mode):
        self.reattach_tree()
        self.filter_tree(self.search_text.get())

    def add_tree(self, root, tree_id):
        if 'children' in root:
            for child in sorted(root['children'].values(), key=lambda c: c["id"]):
                if 'name' not in child:
                    print(f"WARNING: Source {child['id']} lacking name")
                self.tree_view.insert(tree_id, 'end', iid=child['id'], text=child.get('name'))
                self.add_tree(child, child['id'])
        else:
            # leaf
            for year in root['years']:
                self.tree_view.insert(root['id'], 'end', iid=f'{root["id"]}-{year}', text=year),
                if 'indicators' not in root:
                    print(f"WARNING: {root['id']} has no indicators")
                for indicator in sorted(root.get('indicators', []), key=lambda i: i["id"]):
                    if 'shortName' not in indicator:
                        print(f"WARNING: Indicator {indicator['id']} of {root['id']} has no shortName")
                    self.tree_view.insert(f'{root["id"]}-{year}', 'end',
                            iid=f"{root['id']}/{year}/{indicator['id']}",
                            text=indicator.get('tid', indicator['id']),
                        values=(indicator.get('shortName'), indicator.get('longName'), f"{root['id']}/{year}/{indicator.get('tid', indicator['id'])}"))

    def create_widgets(self):
        self.search_text = StringVar()
        self.search_text.trace("w", self.update_search)
        self.search_box = Entry(self, textvariable=self.search_text)
        self.search_box.pack()

        self.tree_view = CheckboxTreeview(self, columns=('nomecurto', 'nomelongo', 'id'))

        self.tree_view.heading('#0', anchor='w', text='Identificador')
        self.tree_view.heading('nomecurto', anchor='w', text='Nome Curto')
        self.tree_view.heading('nomelongo', anchor='w', text='Nome Longo')
        self.tree_view.heading('id', anchor='w', text='Identificador')

        # self.tree_view.column('year', anchor='e')
        # self.tree_view.heading('year', anchor='e', text='Ano')

        self.add_tree(sources_tree, '')

        self.tree_view.pack(fill="both", expand=True)

        self.download_button = Button(self)
        self.download_button['text'] = 'Baixar'
        self.download_button['command'] = self.download
        self.download_button.pack()

        self.progressbar = Progressbar(self)
        self.progressbar.pack()

root = Tk()
app = Application(master=root)
style = Style()
style.theme_use('clam')
style.configure("R.TButton", foreground="red")
root.mainloop()
