import jaydebeapi
from os import path

root = path.dirname(path.abspath(__file__))
ucanaccess_jars = [
    path.join(root, "UCanAccess/ucanaccess-5.0.0.jar"),
    path.join(root, "UCanAccess/lib/commons-lang3-3.8.1.jar"),
    path.join(root, "UCanAccess/lib/commons-logging-1.2.jar"),
    path.join(root, "UCanAccess/lib/hsqldb-2.5.0.jar"),
    path.join(root, "UCanAccess/lib/jackcess-3.0.1.jar"),
]
classpath = ":".join(ucanaccess_jars)

def get_rows_from_access(source, file_format, filename):
    query = source['query']

    conn = jaydebeapi.connect(
        "net.ucanaccess.jdbc.UcanaccessDriver",
        f"jdbc:ucanaccess://{filename}",
        ["", ""],
        classpath
    )
    cur = conn.cursor()
    cur.execute(query)
    yield tuple(name for name, type_code, display_size, internal_size, precision, scale, null_ok in cur.description)
    for row in cur.fetchall():
        yield row
    cur.close()
    conn.close()
