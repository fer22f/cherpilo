import sqlite3
from extra_iter import grouper

connection = sqlite3.connect("db", isolation_level=None)

# Create tables
with connection:
    connection.execute('''
    CREATE TABLE IF NOT EXISTS city (
        id INTEGER,
        name TEXT,
        uf_initials TEXT
    )
    ''')
    connection.execute('''
    CREATE TABLE IF NOT EXISTS alias_city (
        city_id INTEGER,
        name TEXT,
        uf_initials TEXT
    )
    ''')
    connection.execute('''
    CREATE TABLE IF NOT EXISTS source (
        sid INTEGER PRIMARY KEY AUTOINCREMENT,
        id TEXT
    )
    ''')
    connection.execute('''
    CREATE TABLE IF NOT EXISTS indicator (
        sid INTEGER PRIMARY KEY AUTOINCREMENT,
        source_sid INTEGER,
        id TEXT
    )
    ''')
    connection.execute('''
    CREATE TABLE IF NOT EXISTS indicator_city (
        indicator_sid INTEGER,
        city_id INTEGER,
        value TEXT
    )''')

def is_imported(source_id):
    cur = connection.execute('''
    SELECT 1 FROM source
    WHERE id = ?
    ''', (source_id,))
    return bool(cur.fetchone())

def get_city_count():
    cur = connection.execute('''SELECT COUNT(*) FROM city''')
    row = cur.fetchone()
    return row[0] if row else 0

def get_alias_city_count():
    cur = connection.execute('''SELECT COUNT(*) FROM alias_city''')
    row = cur.fetchone()
    return row[0] if row else 0

def get_city_ids():
    cur = connection.execute('''SELECT id FROM city ORDER BY id''')
    while True:
        rows = cur.fetchmany()
        if len(rows) == 0:
            break
        for row in rows:
            sdibgeid, = row
            yield sdibgeid

def insert_cities(iterable):
    cur = connection.cursor()
    cur.execute('SAVEPOINT batch_insert')
    for rows in grouper(iterable, 1000):
        cur.execute('SAVEPOINT item_batch')
        for city_id, name, uf_initials in rows:
            cur.execute('''
            INSERT INTO city (id, name, uf_initials) VALUES (?, ?, ?)
            ''', (city_id, name.upper(), uf_initials.upper()))
        cur.execute('RELEASE item_batch')

    cur.execute('RELEASE batch_insert')

def insert_alias_cities(iterable):
    cur = connection.cursor()
    cur.execute('SAVEPOINT batch_insert')
    for rows in grouper(iterable, 1000):
        cur.execute('SAVEPOINT item_batch')
        for city_id, name, uf_initials in rows:
            cur.execute('''
            INSERT INTO alias_city (city_id, name, uf_initials) VALUES (?, ?, ?)
            ''', (city_id, name.upper(), uf_initials.upper()))
        cur.execute('RELEASE item_batch')

    cur.execute('RELEASE batch_insert')

def get_city_by_name_uf_initials(name, uf_initials):
    cur = connection.execute('''
    SELECT id FROM city
    WHERE name = ? AND uf_initials = ?
    ''', (name.upper(), uf_initials.upper()))
    row = cur.fetchone()
    if not row:
        cur = connection.execute('''
        SELECT city_id FROM alias_city
        WHERE name = ? AND uf_initials = ?
        ''', (name.upper(), uf_initials.upper()))
        row = cur.fetchone()
    return row[0] if row else None

def batch_insert_indicator_city(source_id, iterable):
    indicator_id_to_sid = dict()
    cur = connection.cursor()
    cur.execute('SAVEPOINT batch_insert')
    cur.execute('INSERT INTO source (id) VALUES (?)', (source_id,))
    source_sid = cur.lastrowid

    for rows in grouper(iterable, 10000):
        cur.execute('SAVEPOINT item_batch')

        for indicator_id, city_id, value in rows:
            if value is None: continue
            if indicator_id not in indicator_id_to_sid:
                cur.execute('''
                INSERT INTO indicator (id, source_sid) VALUES (?, ?)
                ''', (indicator_id, source_sid))
                indicator_id_to_sid[indicator_id] = cur.lastrowid
            indicator_sid = indicator_id_to_sid[indicator_id]
            cur.execute('''
            INSERT INTO indicator_city
            (indicator_sid, city_id, value)
            VALUES
            (?, ?, ?)
            ''', (indicator_sid, city_id, value))

        cur.execute('RELEASE item_batch')

    cur.execute('RELEASE batch_insert')

def get_indicators_value_for_cities(indicator_aids, city_ids):
    cur = connection.execute('''
    SELECT
        s.id || '/' || i.id as indicator_aid,
        CAST(city_id AS text),
        ic.value
    FROM indicator_city ic
    INNER JOIN indicator i ON ic.indicator_sid = i.sid
    INNER JOIN source s ON s.sid = i.source_sid
    WHERE ic.city_id IN (''' + ','.join('?' for _ in range(len(city_ids))) + ''')
    AND (s.id || '/' || i.id) IN (''' + ','.join('?' for _ in range(len(indicator_aids))) + ''')
    ORDER BY indicator_aid, city_id
    ''', city_ids + indicator_aids)
    return cur.fetchall()
