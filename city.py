from database import insert_cities, insert_alias_cities, get_alias_city_count, get_city_count
from download import download_and_prepare_file
import unicodedata
import pyexcel

cities_ibge_source = {
    "url": "ftp://geoftp.ibge.gov.br/organizacao_do_territorio/estrutura_territorial/areas_territoriais/2018/AR_BR_RG_UF_MES_MIC_MUN_2018.xls",
    "md5": "c34fbcceb0529ecfb0dbdf5c4b2d11bf",
    "format": "xls",
    "workbook": "AR_BR_MUN_2018",
    "startRow": 1,
    "endRow": 5573
}

toponimic_changes_source = {
    "url": "ftp://geoftp.ibge.gov.br/organizacao_do_territorio/estrutura_territorial/alteracoes_toponimicas_municipais/Alteracoes_Toponimicas_Municipais_2018.xls",
    "md5": "3b956db1b1069d2e039cda60cd42ea7a",
    "format": "xls",
    "workbook": "Planilha1",
    "startRow": 1
}

def load_cities(source, filename):
    rows = pyexcel.iget_array(
        file_name=filename,
        sheet_name=cities_ibge_source['workbook'],
        start_row=source['startRow'],
        row_limit=source['endRow'] - source['startRow'],
        skip_hidden_row_and_column=False
    )
    return ((city_id, name, uf_initials) for _, _, _, uf_initials, city_id, name, _ in rows)

def load_toponimic_changes(source, filename):
    rows = pyexcel.iget_array(
        file_name=filename,
        sheet_name=toponimic_changes_source['workbook'],
        start_row=source['startRow'],
        skip_hidden_row_and_column=False
    )
    return ((state_id + partial_city_id, previous_name, uf_initials) for uf_initials, state_id, partial_city_id, _, previous_name, _, _, _, _, occurence_date in rows)

def strip_accents(text):
    text = unicodedata.normalize('NFD', text)
    text = text.encode('ascii', 'ignore')
    text = text.decode("utf-8")
    return str(text)

def insert_alias_cities_with_unaccented(call):
    insert_alias_cities(call())
    insert_alias_cities(
        (city_id, strip_accents(name), uf_initials)
        for city_id, name, uf_initials in call())

def load_cities_into_database():
    if get_alias_city_count():
        print("Alias cities already populated")
    else:
        print("Inserting alias cities...")
        toponimic_changes_filepath, _ = download_and_prepare_file(toponimic_changes_source)
        insert_alias_cities_with_unaccented(
            lambda: load_toponimic_changes(toponimic_changes_source, toponimic_changes_filepath))

        extra_cities = [
            # Santa Terezinha - BA
            (2928505, "Santa Teresinha", "BA"),
            # Pindaré-Mirim - MA
            (2108504, "Pindaré Mirim", "MA"),
            # Nova Canaã do Norte - MT
            (5106216, "Nova Canãa do Norte", "MT"),
            # Batayporã - MS
            (5002001, "Bataiporã", "MS"),
            # Dona Eusébia - MG
            (3122900, "Dona Euzébia", "MG"),
            # Gouveia - MG
            (3127602, "Gouvêa", "MG"),
            # Queluzito - MG
            (3153806, "Queluzita", "MG"),
            # Santa Rita de Ibitipoca - MG
            (3159407, "Santa Rita do Ibitipoca", "MG"),
            # Chiapetta - RS
            (4305405, "Chiapeta", "RS"),
            # Pantano Grande - RS
            (4313953, "Pântano Grande", "RS"),
            # Varre-Sai - RJ
            (3306156, "Varre e Sai", "RJ"),
            (3306156, "Varre Sai", "RJ"),
            # Olho d'Água do Borges - RN
            (2408409, "Olho-d'Água do Borges", "RN"),
            # Mogi Guaçu - SP
            (3530706, "Moji-guaçu", "SP"),
            # Pedro Avelino - RN
            (2409704, "Pedra Avelino", "RN"),
            (2409704, "Pedaa Avelino", "RN"),
            # Piacatu - SP
            (3537701, "Piacutu", "SP"),
            # Pindorama do Tocantins - TO (mudança toponímica)
            (1717008, "Pindorama de Goiás", "TO"),
            # Santo Antônio do Aracanguá - SP
            (3548054, "Santo Antônio do Aracangua", "SP"),
            # Acarape - CE
            (2300150, "Acarapé", "CE"),
            # Pirapora do Bom Jesus - SP
            (3539103, "Pirapora do Bom Jeus", "SP"),
            # Paço do Lumiar - MA
            (2107506, "Poco do Lumiar", "MA"),
            # Sítio Novo - RN
            (2413706, "Sito Novo", "RN"),
            # Abelardo Luz - SC
            (4200101, "Aberlardo Luz", "SC"),
            # Amparo do Serra - MG
            (3102506, "Amparo da Serra", "MG"),
            # Balneário Camboriú - SC
            (4202008, "Balneario de Camboriu", "SC"),
            # Alto Horizonte - GO
            (5200555, "Alto Horizonte", "CE"),
            # Alvorada D'Oeste - RO
            (1100346, "Alvorada do Oeste", "RO"),
            # Cerejeiras - RO
            (1100056, "Cerejeira", "RO"),
            # Suzanápolis - SP
            (3552551, "Suzanopolis", "SP"),
            # Cachoeira Paulista - SP
            (3508603, "Cach.Paulista", "SP"),
            # Morro da Garça - MG
            (3143609, "Morro da Carca", "MG"),
            # Tuiuti - SP
            (3554953, "De Tuiuti", "SP"),
            # Mineiros do Tietê - SP
            (3529807, "Mineir.do Tiete", "SP"),
            # Bom Sucesso de Itararé - SP
            (3507159, "Bom Sucesso do Itarare", "SP"),
            # Vila Bela da Santíssima Trindade - MT
            (5105507, "Vl Bela da Santissima Trindade", "MT"),
            # Cacoal - RO
            (1100049, "Cocal", "RO"),
            # Bom Jesus dos Perdões - SP
            (3507100, "B.Jesus Perdoes", "SP"),
            # Cerro Corá - RN
            (2402709, "Cerro-Cora", "RN"),
            # Santa Carmem - MT
            (5107248, "Santa Carmen", "MT"),
            # Cabrália Paulista - SP
            (3508306, "Cabral.Paulista", "SP"),
            # Boa Esperança do Sul - SP
            (3506706, "Boa Esp.do Sul", "SP"),
            # Ferraz de Vasconcelos - SP
            (3515707, "F.de Vasconcelos", "SP"),
            # Gouvelândia - GO
            (5209150, "Gouvelandia/GO", "GO"),
            # Atalaia do Norte - AM
            (1300201, "Atalaia do Nrote", "AM"),
            # Guarinos - GO
            (5209457, "Guarinos/GO", "GO"),
            # Cássia dos Coqueiros - SP
            (3510906, "C.dos Coqueiros", "SP"),
            # Bragança Paulista - SP
            (3507605, "Brag.Paulista", "SP"),
            # Rodrigues Alves - AC
            (1200427, "Rodrugues Alves", "AC"),
            # Dois Irmãos do Tocantins - TO
            (1707207, "Dois Irmaos", "TO"),
            # Espírito Santo do Pinhal - SP
            (3515186, "Esp.S.do Pinhal", "SP"),
            # Alto Boa Vista - MT
            (5100359, "Alto da Boa Vista", "MT"),
            # Barão de Cotegipe - RS
            (4301701, "Barao do Cotegipe", "RS"),
            # Campo Limpo Paulista - SP
            (3509601, "Cam.Lim.Paulista", "SP"),
            # Campos Novos Paulista - SP
            (3509809, "Cam.Nov.Paulista", "SP"),
            # Campo Alegre de Goiás - GO
            (5204805, "Campo Alegre de Goais", "GO"),
            # Santa Cruz de Monte Castelo - PR
            (4123303, "Santa Cruz do Monte Castelo", "PR"),
            # São João da Urtiga - RS
            (4318424, "Sao Joao do Urtiga", "RS"),
            # São José do Vale do Rio Preto - RJ
            (3305158, "Sao Jose do Valedo Rio Preto", "RJ"),
            # Nova Guataporanga - SP
            (3533106, "Nov.Guataporanga", "SP"),
            # Manoel Vitorino - BA
            (2920403, "Manoel Vitorinho", "BA"),
            # Eusébio - CE
            (2304285, "Euzebio", "CE"),
            # Caldas Brandão - PB
            (2503803, "Caldas de Brandao", "PB"),
            # Guarani das Missões - RS
            (4309506, "Grarani das Missoes", "RS"),
            # Cachoeira de Pajeú - MG
            (3102704, "Cachoeira do Pajeu", "MG"),
            # Marechal Thaumaturgo - AC
            (1200351, "Marechal Taumaturgo", "AC"),
            # Monte Alegre do Sul - SP
            (3531209, "Mont.Aleg.do Sul", "SP"),
            # São Francisco de Sales - MG
            (3161304, "Sao Franciscode Sales", "MG"),
            # Comendador Levy Gasparian - RJ
            (3300951, "Comendador Levy Gasperian", "RJ"),
            # Jucuruçu - BA
            (2918456, "Jucururu", "BA"),
            # Itapecerica da Serra - SP
            (3522208, "Itapec.da Serra", "SP"),
            # Laranjal Paulista - SP
            (3526407, "Laranj.Paulista", "SP"),
            # Natividade da Serra - SP
            (3532306, "Nativ.da Serra", "SP"),
            # Nova Bandeirantes - MT
            (5106158, "Nova Bandeirante", "MT"),
            # Patrocínio Paulista - SP
            (3536307, "Patroc.Paulista", "SP"),
            # Cândido Rodrigues - SP
            (3510104, "Cand.Rodrigues", "SP"),
            # Cristais Paulista - SP
            (3513207, "Crist.Paulista", "SP"),
            # Governador Celso Ramos - SC
            (4206009, "Gov.Celso Ramos", "SC"),
            # Gracho Cardoso - SE
            (2802601, "Graccho Cardoso", "SE"),
            # Ilha de Itamaracá - PE
            (2607604, "Itamaraca", "PE"),
            # Nova Tebas - PR
            (4117271, "Novas Tebas", "PR"),
            # São Miguel da Boa Vista - SC
            (4217154, "Sao Miguel da Boa Visa", "SC"),
            # Sud Mennucci - SP
            (3552304, "Sud Menucci", "SP"),
            # Santa Terezinha de Goiás - GO
            (5219704, "Terezinha de Goias", "GO"),
            # Terezópolis de Goiás - GO
            (5221197, "Terezoplis de Goias", "GO"),
            # Carnaíba - PE
            (2603900, "Carnaibas", "PE"),
            # Brazabrantes - GO
            (5203609, "Brasabrantes", "GO"),
            # Lajedo do Tabocal - BA
            (2919058, "Lagedo do Tabocal", "BA"),
            # Palmares Paulista - SP
            (3535101, "Palmar.Paulista", "SP"),
            # Jardim de Angicos - RN
            (2405504, "Jardim dos Angicos", "RN"),
            # Monte Azul Paulista - SP
            (3531506, "M.Azul Paulista", "SP"),
            # Lajeado - TO
            (1712009, "Lajeado do Tocantins", "TO"),
            # Parauapebas - PA
            (1505536, "Paraupebas", "PA"),
            # Tejuçuoca - CE
            (2313351, "Tejussuoca", "CE"),
            # Paraguaçu Paulista - SP
            (3535507, "Parag.Paulista", "SP"),
            # Aurora do Tocantins - TO (mudança toponímica)
            (1702703, "Aurora do Norte", "TO"),
            # Bernardino de Campos - SP
            (3506300, "Bern.de Campos", "SP"),
            # Januário Cicco - RN (mudança toponímica)
            (2405306, "Boa Saude", "RN"),
            # Augusto Severo - RN (mudança toponímica)
            (2401305, "Campo Grande", "RN"),
            # Canguçu - RS
            (4304507, "Cangussu", "RS"),
            # Cariri do Tocantins - TO
            (1703867, "Caririr do Tocantins", "TO"),
            # Deputado Irapuan Pinheiro - CE
            (2304269, "Deputado Irapua Pinheiro", "CE"),
            # Entre-Ijuís - RS
            (4306932, "Entre Ijuis", "RS"),
            # Entre Rios de Minas - MG
            (3123908, "Entre Rio de Minas", "MG"),
            # Indaiatuba - SP
            (3520509, "Idaiatuba", "SP"),
            # Itacurubi - RS
            (4310553, "Itacuruibi", "RS"),
            # Itaporã do Tocantins - TO (mudança toponímica)
            (1711100, "Itapora de Goias", "TO"),
            # Juara - MT
            (5105101, "Jaura", "MT"),
            # Jucati - PE
            (2608255, "Jucabi", "PE"),
            # Lajeado Grande - SC
            (4209458, "Lageado Grande", "SC"),
            # Luiziânia - SP
            (3527702, "Luisiania", "SP"),
            # Mirante do Paranapanema - SP
            (3530201, "Mir.Parapanema", "SP"),
            # Passabém - MG
            (3147501, "Passagem", "MG"),
            # Presidente Venceslau - SP
            (3541505, "Presidente Wenceslau", "SP"),
            # São João da Boa Vista - SP
            (3541505, "S.J.da Boa Vista", "SP"),
            # Salmourão - SP
            (3545100, "Salmorao", "SP"),
            # Santana do São Francisco - SE
            (2806404, "Santana de Sao Francisco", "SE"),
            # São Fidélis - RJ
            (3304805, "Sao Fideles", "RJ"),
            # São João de Iracema - SP
            (3549250, "Sao Joao do Iracema", "SP"),
            # São João do Rio do Peixe - PB
            (2500700, "Sao Joao do Rio Peixe", "PB"),
            # São Luiz do Norte - GO
            (5220157, "Sao Luis do Norte", "GO"),
            # Senador Modestino Gonçalves - MG
            (3165909, "Senador Modesto Goncalves", "MG"),
            # Taquaruçu do Sul - RS
            (4321329, "Taquaracu do Sul", "RS"),
            # Novo Itacolomi - PR
            (4117297, "Itacolomi", "PR"),
            # Alto Paraíso - PR (mudança toponímica de Vila Alta)
            (4128625, "Vila Alia", "PR"),
            # Axixá do Tocantins - TO
            (1702901, "Axixa de Tocantins", "TO"),
            # Nova Laranjeiras - PR
            (4117057, "Nova Laranjeira", "PR"),
            # Rancho Alegre D'Oeste - PR
            (4121356, "Rancho Alegre D,Oeste", "PR"),
            # Novo Horizonte do Oeste - RO
            (4121356, "Novo Horizonte D`Oeste", "RO"),
            # Igarapé-Miri - PA
            (1503309, "Igarape-Mirim", "PA"),
            # Alto Jequitibá - MG
            (3153509, "Alto Jequitiba (Presidente Soares)", "MG"),
            # Boa Esperança do Iguaçu - PR
            (4103024, "Boa Esperanaca do Iguacu", "PR"),
            # Campos dos Goytacazes - RJ
            (3301009, "Campos_dos Goytacazes", "RJ"),
            # Carnaubeira da Penha - PE
            (2603926, "Carnaubeiras da Penha", "PE"),
            # Colinas do Tocantins - TO
            (1705508, "Colinas de Tocantins", "TO"),
            # Lagoa do Tocantins - TO
            (1711951, "Lagoa do Tocantis", "TO"),
            # Ferreira Gomes - AP
            (1600238, "Ferreira  Gomes", "AP"),
            (1600238, "Ferreira A86Gomes", "AP"),
            (1600238, "Ferreira__Gomes", "AP"),
            # Dois Irmãos do Tocantins - TO
            (1707207, "Dois Irmaos_do Tocantins", "TO"),
            # Lagoa Alegre - PI
            (2205557, "Logoa Alegre", "PI"),
            # Iporã do Oeste - SC
            (4207650, "Ipora D'Oeste", "SC"),
            # Curralinho - PA
            (1502806, "Curralino", "PA"),
            # Elisiário - SP
            (3514924, "Elisario", "SP"),
            # Glória D'Oeste - MT
            (5103957, "Gloria D Oeste", "MT"),
            # Gouvelândia - GO
            (5209150, "Gouverlandia", "GO"),
            # Itaguaçu da Bahia - BA
            (2915353, "Itaguacu da Baia", "BA"),
            # Jaboatão dos Guararapes - PE
            (2607901, "Jaboatao_dos Guararapes", "PE"),
            # Jaú do Tocantins - TO
            (1711506, "Jau do Tocantis", "TO"),
            # Lambari D'Oeste - MT
            (5105234, "Lambari D Oeste", "MT"),
            # Mathias Lobato - MG
            (3171501, "Mathias Lobato (Vila Matias)", "MG"),
            # Parisi - SP
            (3536257, "Parasi", "SP"),
            # Pau D'Arco - PA
            (1505551, "Pau D Arco", "PA"),
            # Virmond - PR
            (4128658, "Virmono", "PR"),
            # Aparecida do Rio Negro - TO
            (1701101, "Aparecida do Rio Negro", "RO"),
            # Santa Tereza do Tocantins - TO
            (1719004, "Santa Teresa do Tocantins", "TO"),
            # São Domingos do Sul - RS
            (4318051, "Sao Domingis do Sul", "RS"),
            # Senador Amaral - MG
            (3165578, "Sendor Amaral", "MG"),
            # Terezópolis de Goiás - GO
            (3165578, "Teresopolis de Goias", "GO"),
            # Teresina de Goiás - GO
            (5221080, "Terezina de Goias", "GO"),
            # Wenceslau Braz -  PR
            (4128500, "Venceslau Braz", "PR"),
            # Pau D'Arco - TO
            (1716307, "Pau D Arco", "TO"),
            # São Caitano - PE
            (2613107, "Sao Caetano", "PE"),
            # São Gonçalo do Rio Preto - MG
            (3125507, "Sao Goncalo Do Rio Preto (Ex-Felisb.Caldeira)", "MG"),
            # Serra Caiada - RN
            (2410306, "Serra Caiada (Ex-Presidente Juscelino)", "RN"),
            # Serra do Navio - AP
            (1600055, "Serra do Navio (Ex-Agua Branca do Amapari)", "AP"),
            # Novo Horizonte do Oeste - RO
            (1100502, "Novo Horizonte D`Oeste (Ex-Cacaieiros)", "RO"),
            # Cachoeira de Pajeú - MG
            (3102704, "Cachoeira do Pajeu (Ex-Andre Fernandes)", "MG"),

            # Problemas com Pará e Rondônia

            # Rolim de Moura - RO
            (1100288, "Rolim de Moura", "PA"),
            # Machadinho D'Oeste - RO
            (1100130, "Machadinho D'Oeste", "PA"),
            # Pimenta Bueno - RO
            (1100189, "Pimenta Bueno", "PA"),
            # Guajará-Mirim - RO
            (1100106, "Guajara-Mirim", "PA"),
            # Costa Marques - RO
            (1100080, "Costa Marques", "PA"),
            # Santa Luzia D'Oeste - RO
            (1100296, "Santa Luzia D'Oeste", "PA"),
            # Nova Mamoré - RO (mudança toponímica)
            (1100338, "Vila Nova do Mamore", "PA"),
            # Colorado do Oeste - RO
            (1100064, "Colorado do Oeste", "PA"),
            # Ariquemes - RO
            (1100023, "Ariquemes", "PA"),
            # Presidente Médici - RO
            (1100254, "Presidente Medici", "PA"),
            # Nova Brasilândia D'Oeste - RO
            (1100148, "Nova Brasilandia D'Oeste", "PA"),
            # Jaru - RO
            (1100114, "Jaru", "PA"),
            # Ji-Paraná - RO
            (1100122, "Ji-Parana", "PA"),
            # Cabixi - RO
            (1100031, "Cabixi", "PA"),
            # Cerejeiras - RO
            (1100056, "Cerejeira", "PA"),
            # Alvorada D'Oeste - RO
            (1100346, "Alvorada do Oeste", "PA"),
            # Vilhena - RO
            (1100304, "Vilhena", "PA"),
            # Cacoal - RO
            (1100049, "COCAL", "PA"),
            # Alta Floresta D'Oeste - RO
            (1100015, "Alta Floresta D'Oeste", "PA"),
            # Espigão D'Oeste - RO
            (1100098, "Espigao D'Oeste", "PA"),
            # Ouro Preto do Oeste - RO
            (1100155, "Ouro Preto do Oeste", "PA"),
            # São Miguel do Guaporé - RO
            (1100320, "Sao Miguel do Guapore", "PA"),
        ]
        insert_alias_cities_with_unaccented(lambda: extra_cities)

    if get_city_count():
        print("Cities already populated")
    else:
        print("Inserting cities...")
        cities_ibge_filepath, _ = download_and_prepare_file(cities_ibge_source)
        insert_cities(load_cities(cities_ibge_source, cities_ibge_filepath))
        insert_alias_cities(
            (city_id, strip_accents(name), uf_initials)
            for city_id, name, uf_initials
            in load_cities(cities_ibge_source, cities_ibge_filepath) if strip_accents(name) != name)
